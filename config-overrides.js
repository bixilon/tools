const webpack = require('webpack');
const HtmlReplaceWebpackPlugin = require('html-replace-webpack-plugin')


module.exports = function override(config, env) {
    config.resolve.fallback = {
        //   url: require.resolve('url'),
        //   fs: require.resolve('fs'),
        //   assert: require.resolve('assert'),
        crypto: require.resolve('crypto-browserify'),
        //   http: require.resolve('stream-http'),
        //   https: require.resolve('https-browserify'),
        //   os: require.resolve('os-browserify/browser'),
        buffer: require.resolve('buffer'),
        stream: require.resolve('stream-browserify'),
        vm: require.resolve('vm-browserify'),
    };
    if (config.plugins === undefined) {
        config.plugins = []
    }
    let commit = process.env.CI_COMMIT_SHA || require('child_process').execSync('git rev-parse HEAD')
    let commit_short = process.env.CI_COMMIT_TAG || process.env.CI_COMMIT_SHORT_SHA || require('child_process').execSync('git rev-parse --short HEAD')

    config.plugins.push(new HtmlReplaceWebpackPlugin([
        {
            pattern: '{GIT_COMMIT}',
            replacement: commit
        },
        {
            pattern: '{GIT_COMMIT_SHORT}',
            replacement: commit_short
        }
    ]))

    return config;
}
