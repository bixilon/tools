# General Utility

This application contains some general utility that I often need. Written with react and will be deployed to [tools.bixilon.de](https://tools.bixilon.de).

## Building & Running

This is a simple react application, just run `yarn start` to run it.

## License

This tool is licensed under the terms of the [GPLv3](LICENSE.md).

The following items are excluded from the license:

- all logos and images
- bx5 theme (stylesheet)
