import {Length} from "./Length";
import {render, screen} from '@testing-library/react'

export {}

test('render input', () => {
    render(<Length/>);
    const element = screen.getByText(/Input/i);
    expect(element).toBeInTheDocument();
});

/*
// testing in react sucks? https://stackoverflow.com/questions/48180499/testing-onchange-function-in-jest
test('6 char string', () => {
    const view = render(<Length/>);
    // eslint-disable-next-line testing-library/no-container,testing-library/no-node-access
    const input = view.container.getElementsByTagName("textarea")[0]
    input.value = "6 chars"
    const event = {target: { value: '6 chars' }} as React.ChangeEvent<HTMLTextAreaElement>
    // @ts-ignore
    input.onchange(event)
    // eslint-disable-next-line testing-library/no-container,testing-library/no-node-access
    const count = view.container.getElementsByClassName("counter")[0]
    expect(count).toHaveTextContent("6")
});
 */
